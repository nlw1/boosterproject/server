# Booster - Backend

Um projeto pensado para melhorar o integração entre empresas que cuidam do meio ambiente e a sociedade.

## Ferramentas indicadas
### Visual Studio Code
- Excelente editor de texto.
- Há a possibilidade de instalação de diversas extensões que agragarão bastante para seu desenvolvimento.

### Algumas extensões utilizadas no VSCode
- Drácula (Para estilização da aparência - fica mais escuro)
- Material Icon Theme (Estiliza a apresentação dos pacotes a depender do tipo de arquivo na aba de exploração)

### Insomnia
Download: https://insomnia.rest/download/core/?&ref=

- Ferramenta para realizar testes com as requisições HTTP ao seu backend.


# O que teremos no nosso backend
- Regras de negócio;
- Conexão com o banco de dados;
- Envio de e-mail ou conexão com servços externos;
- Auntenticação e autorização dos usuários;
- Criptografia e segurança.

# Funcionalidades do sistema
- Para a interface web
    - Cadastro de ponto de coleta
    - Listar os itens de uma coleta
- Para a interface mobile
    - Listar pontos (filtro por estado/cidade/itens)
    - Listar um ponto de coleta específico



# Tecnologias utilizadas

## Node.js

- Nodejs é uma plataforma de aplicação baseado em javascript com diversas funcionalidades que facilitam o desenvolvimento de uma api.
- Desenvolvida em 2009 pelo programador Ryan Dahl.
- Os programas desenvolvidos com o nodejs serão compilados e interpretados pela máquina virtual v8, a mesma que o Google utiliza para executar javaScript no Chrome.
- Node.js é uma tecnologia assíncrona. Ou seja, cada requisição não bloqueia seu processoo. Dessa forma é possível atender um grande volume de requisições.
- Utilizaremos o node para criar nosso backend e nossa API REST.
- Essa API poderá ser acessada tanto pelo REACT JS, quanto pelo REACT NATIVE.

--- 

## SQLite
- Para essa aplicação de exemplo iremos utilizar o SQLite.
- É uma biblioteca escrita em C que permite a disponibilização de um banco de dados na própria aplicação.
- É um banco de dados embutido que pode ser utilizado para pequenas aplicações.

#### Plugin VSCode
- Ubuntu 18.04.4 LTS primeiro instalar com apt-get
    - sudo apt-get install sqlite3.
- Instalar o plugin SQLite no Visual Studio Code.
- Esse plugin permite-nos visualizar as tabelas e dados do banco de dados.

#### Tabelas
- points (Pontos de coleta) - N
    - image
    - name
    - email
    - whatsapp
    - latitude
    - longitude
    - city
    - uf
- items (Quais são os itens para a coleta) - N
    - title
    - image

#### Relacionamentos
- N points tem N Items
- Criação de uma tabela pivot
    - point_items (Quais são os itens que esse ponto coleta)
        - point_id
        - item_id


## TypeScript
- Iremos utilizar o typeScript, pois ele nos fornece algumas facilidades em relação ao desenvolvimento.
    - Intellisense
    - Facilidade de entendimento do código pela equipe, por seguir um padrão.
    - O mercado está usando cada vez mais o typeScript.
- É important deixar claro que o TypeScript não substitui o JavaScript. Na verdade aquele foi feito baseado neste.
    - O código feito em typeScript será convertido para JS quando for executado.

## Knex
- Uma biblioteca capaz de desenvolver queries sql em javascript.
- Cria um baixo acoplamento em relação ao SGDB utilizado.
- Link: http://knexjs.org/

### Knex Migrations
- As migrations são utilizadas para realizar um controle de versão do nosos banco de dados.
- Podemos fazer upgrade e rollbacks com facilidade.
- Histórico de banco de dados.
- A ordem dos arquivos criados dentro da pasta migrations, será a ordem que elas serão executadas no Banco de Dados.
- Tomar cuidado, por exemplo, para que a tabela n-n não tente ser criada antes das tabelas principais.
- Para isso vamos colocar sempre um número antes do nome da migration, para que fiquem ordenadas de acordo com nossa necessidade.
- É preciso criar na raiz do projeto um arquivo knexfile.ts

### Knex Transaction
- Recurso muito interessante do Knex.
- Permite que possamos garantir a atomicidade e consistêncida das nossas transações.

#### ACID
- É um conjunto de propriedades de transação em banco de dados.
- No contexto de banco de dados, transação é uma sequência de operações de banco de dados que satisfaz as propriedades ACID e, portanto, pode ser percebida como uma operação lógica única sobre os dados. 
Fonte: https://pt.wikipedia.org/wiki/ACID

##### Atomicidade
- Trata o trabalho como parte indivisível (atômico). 
- A transação deve ter todas as suas operações executadas em caso de sucesso ou, em caso de falha, nenhum resultado de alguma operação refletido sobre a base de dados. 
- Ou seja, após o término de uma transação (commit ou rollback), a base de dados não deve refletir resultados parciais da transação. 
Fonte: https://pt.wikipedia.org/wiki/ACID

##### Consistência
- A execução de uma transação deve levar o banco de dados de um estado consistente a um outro estado consistente, ou seja, uma transação deve respeitar as regras de integridade dos dados (como unicidade de chaves, restrições de integridade lógica, etc.). 
Fonte: https://pt.wikipedia.org/wiki/ACID

#### Executando nossa migration
- npx knex migrate:latest --knexfile knexfile.ts migrate:latest


## Express.js
- Framework minimalista e rápido para Node.js.
- Perfeito para a criação de APIs REST.
- Nos possibilita criar rotas para serem acessadas através dos métodos HTTP bem como middlewares para controles necessários.

#### É necessário informar que o Express irá trabalhar com o JSON
- app.use(express.json());

## Recurso do javascript - path
- Permite definir ou buscar caminhos entre os diretórios de forma global.
- Ou seja, independente do sistema operacional que eu estiver usando ele irá funcionar.

# Inicalizando o nosso backend

### npm init -y
- Esse comando já cria nosso package.json padrão.
- package.json:
    - é o pacote que fará o gerenciamento de todas as dependências do projeto.

### ts-node-dev
- Ficará observando nossa aplicação.
- Sempre que fizermos uma alteração, ele reinicia nossa API.

### npx tsc --init
- Cria nosso arquivo de configuração para podermos trabalhar com o typescript.

### npx ts-node src/server.ts
- Pacote que possibilita a execução do express.

# Estrutura da aplicação

### SRC
- src (pasta em que ficará todo o código)

### Rotas
- Endereço completo para uma requisição - Método HTTP + url - Ex: GET localhost:3333/{recurso}
    - GET: Buscar uma ou mais informações do backend.
    - POST: Criar uma nova informação no backend.
    - PUT: Atualizar uma informação no backend.
    - DELETE: Deletar.

##### Recurso
- Qual a entidade estamos acessando do sistema
    - Usuários - {rota}/users
    - Produtos - {rota}/produtos

##### Request Params
- Quando quero buscar um recurso específico através de um identificador
    - Ex: {rota}/{recurso}/identificador - localhost:3333/users/5    
- Utilizado também quandpo quero fazer uma alteração ou deletar um recurso específico de uma rota.
- Geralmente são obrigatórios, ou a rota não vive.

##### Query Params
- São parâmetros que vem na própria rota.
- Geralmente são opcionais.
- Utilizados para fazer filtros, paginação, etc.

##### Request Body Params
- Parâmetros para a criação/atualização de informações.

### Controllers
- Pasta criada para desaclopar um pouco nossa aplicação

# Configurando o CORS - Cross-origin resource sharing
- Permite-nos configurar quais endereços de ip terão acesso à nossa aplicação.

# Multer
- Trabalhando com imagens.
- Criamos um arquivo de configuração mostrando onde nossas imagens serão salvas.
- Importamos essa configuração nas rotas e criamos uma constante instanciando o multer recebendo esse arquivo de configuração.
- Na rotal em que iremos fazer o upload colocamo nossa constante chamando o método single ou multiple a depender da quantidade de fotos.
- Json não consegue carregar imagens. Para isso usaremos o multipart form data.
- fileFilter - dá para escolher para quais tipos de arquivo pode ser feito o upload.

# Docker
- Criar o Dockerfile.
- docker build -t backend-ecoleta .
- docker run -d -p 3333:3333 backend-ecoleta
- Testar a rota de health check (localhos:3333/)
- docker tag 7860771bfcc4 localhost:5000/backend-ecoleta:1.1.0
- docker push