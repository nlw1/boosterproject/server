![alt text](https://helm.sh/img/helm.svg)

# Criando nosso chart para o frontend
## Helm create
- helm create frontend
- Esse comando vai criar os arquivos com a estrutura necessária para o kubernetes.

## Vou colocar aqui como ficou inicialmente três arquivos importantes:

### values.yaml
- Armazenará todos nossos valores que serão referenciados nos outros arquivos.
```
# Default values for backend-ecoleta.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: localhost:5000/backend-ecoleta
  tag: 1.1.0
  pullPolicy: IfNotPresent

imagePullSecrets: []
nameOverride: ""
fullnameOverride: ""

serviceAccount:
  # Specifies whether a service account should be created
  create: true
  # The name of the service account to use.
  # If not set and create is true, a name is generated using the fullname template
  name: ""

podSecurityContext: {}
  # fsGroup: 2000

securityContext: {}
  # capabilities:
  #   drop:
  #   - ALL
  # readOnlyRootFilesystem: true
  # runAsNonRoot: true
  # runAsUser: 1000

service:
  type: NodePort
  port: 80
  targetPort: 3333
  nodePort: 30333

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []

  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #   cpu: 100m
  #   memory: 128Mi
  # requests:
  #   cpu: 100m
  #   memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}
```

### Chart.yaml
```
apiVersion: v1
appVersion: "1.0"
description: A Helm chart for Ecoleta backend
name: backend-ecoleta
version: 1.1.0
```

### deplyment.yaml
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "backend-ecoleta.fullname" . }}
  labels:
{{ include "backend-ecoleta.labels" . | indent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "backend-ecoleta.name" . }}
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: {{ include "backend-ecoleta.name" . }}
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ template "backend-ecoleta.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: {{ .Values.service.targetPort }}
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: {{ .Values.service.targetPort }}
          readinessProbe:
            httpGet:
              path: /
              port: {{ .Values.service.targetPort }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
```

# Install helm
- Dentro da pasta onde está o seu chart execute: helm install . --name backend-ecoleta
- Qualquer erro use: helm delete backend-ecoleta --purge
- Feito isso ajuste os erros e crie novamente.

## Acessando a aplicação
- Para acessar: localhost:30333
- Sua aplicação já está acessível em um cluster kubernetes.