// Importando com a letra maiúscula para poder utilizar do Intellisense que o typscript proporciona
import Knex from 'knex';

// Realizar as operações necessárias no banco de dados
export async function up(knex: Knex) {

    // CRIAR A TABELA
    return knex.schema.createTable('point_items', table => {
        table.increments('id').primary();
        table.integer('point_id').notNullable().references('id').inTable('points');
        table.integer('item_id').notNullable().references('id').inTable('items');

    });
}

// Realizar Rollbacks
export async function down(knex: Knex) {
    
    // REMOVER A TABELA DO BANCO DE DADOS
    return knex.schema.dropTable('point_items');
}
