// Importando com a letra maiúscula para poder utilizar do Intellisense que o typscript proporciona
import Knex from 'knex';

// Realizar as operações necessárias no banco de dados
export async function up(knex: Knex) {

    // CRIAR A TABELA
    return knex.schema.createTable('items', table => {
        table.increments('id').primary();
        table.string('image').notNullable();
        table.string('title').notNullable();

    });
}

// Realizar Rollbacks
export async function down(knex: Knex) {
    
    // REMOVER A TABELA DO BANCO DE DADOS
    return knex.schema.dropTable('items');
}